#include "velocity.h"
#include<time.h>
#include<math.h>

#define sign_function(x, y) (y < 0.0 ? -x : x)

/*
*velocity verlet algorithm
*
*/
cinet_t compute_velocity_verlet(particles_t *p, vec_t *tv, int N, cinet_t cn, char *fname_out, int n_step)
{

    double **r;
    lennard_t js;

    //update particles
    p = update_particle_data(p, tv, N);
    
    //distance
    r = compute_distance(p);
    /*Part for compute  Lennard Jones periodical*/
    js = compute_Lennard_Jones_periodic(p, r, N);
    
    /*Compute moment
    */
    for (int i = 0; i < p->N_particles_total; i++)
    {
        cn.mi[i].mx -= 0.5 * dt *CONVERSION_FORCE *  js.Fsum[i].fx;
        cn.mi[i].my -= 0.5 * dt *CONVERSION_FORCE *  js.Fsum[i].fy;
        cn.mi[i].mz -= 0.5 * dt *CONVERSION_FORCE *  js.Fsum[i].fz;
    }

    //update position
    for (int i = 0; i < p->N_particles_total; i++)
    {
       p[i].coord.x += (cn.mi[i].mx/ M_i) * dt ;
       p[i].coord.y += (cn.mi[i].my/ M_i) * dt ;
       p[i].coord.z += (cn.mi[i].mz/ M_i) * dt;
    }
    //write output
    write_output_pdb(L, L, L, fname_out, p, n_step);

    p = update_particle_data(p, tv, N_sym);
    r = compute_distance(p);
    js = compute_Lennard_Jones_periodic(p, r, N);
    //verication des sommes des forces
    //lennard_verify(js, p);
    
    //update moment 
    for (int i = 0; i < p->N_particles_total; i++)
    {
        cn.mi[i].mx -= 0.5 * dt *CONVERSION_FORCE *  js.Fsum[i].fx;
        cn.mi[i].my -= 0.5 * dt *CONVERSION_FORCE *  js.Fsum[i].fy;
        cn.mi[i].mz -= 0.5 * dt *CONVERSION_FORCE *  js.Fsum[i].fz;
    }

    cn.U = js.Ulj;
    //free
    free_distance(r,p);
    free_lennard(js,p);

    return cn;
}

/*initialisation moment cinetique
*params: particles p
*/
cinet_t init_moment_cinetique(particles_t *p)
{
    cinet_t cn;
    double c, s;

    cn.mi = (moment_t*)malloc(sizeof(moment_t) * p->N_particles_total);

    srand(time(NULL));

    //init moment
    for (int i = 0; i < p->N_particles_total; i++)
    {
        cn.mi[i].mx = 0.0;
        cn.mi[i].my = 0.0;
        cn.mi[i].mz = 0.0;
    }

    for (int i = 0; i < p->N_particles_total; i++)
    {
        //component in x
        c = rand() / ((double)RAND_MAX);
        s = rand() / ((double)RAND_MAX);
        cn.mi[i].mx = sign_function(1.0, 0.5 - s) * c;

        //component in y
        c = rand() / ((double)RAND_MAX);
        s = rand() / ((double)RAND_MAX);
        cn.mi[i].my = sign_function(1.0, 0.5 - s) * c;

        //component in z
        c = rand() / ((double)RAND_MAX);
        s = rand() / ((double)RAND_MAX);
        cn.mi[i].mz = sign_function(1.0, 0.5 - s) * c;
    }
        cn.Ec = 0.0;
        cn.Tc = 0.0;
        cn.Ndl = 0;
        cn.U = 0.0;

    return cn;
}

/*
*compute energie cinetique
*params: in moment
*/
cinet_t compute_cinetique_energie(cinet_t cn, particles_t *p)
{
    double tmp = 0;

    cn.Ndl = 3 * p->N_particles_total -3;

    for (int i = 0; i < p->N_particles_total; i++)
    {
        tmp += (cn.mi[i].mx * cn.mi[i].mx + cn.mi[i].my * cn.mi[i].my + cn.mi[i].mz * cn.mi[i].mz) / M_i;
    }
    //energie cinetique
    cn.Ec = tmp / (2* CONVERSION_FORCE);
    
    //Temperature cinetique
    cn.Tc =  (1/(cn.Ndl * CONSTANT_R))*cn.Ec ;

    return cn;
}

/*
*first recalibrated 
*/
cinet_t compute_first_recalibrated(cinet_t cn, particles_t *p)
{
    double Ra;

    Ra = cn.Ndl * CONSTANT_R * T0 / cn.Ec;

    for (int i = 0; i < p->N_particles_total; i++)
    {
        //component in x
        cn.mi[i].mx = Ra * cn.mi[i].mx;
        //component in y
        cn.mi[i].my = Ra * cn.mi[i].my;
        //component in z
        cn.mi[i].mz = Ra * cn.mi[i].mz;
    }
    
    return cn;
}
/*
*second recalibrated
*/
cinet_t compute_second_recalibrated(cinet_t cn, particles_t *p)
{
    double Px = 0, Py = 0, Pz =0;
    //correction
    for (int i = 0; i < p->N_particles_total; i++)
    {
        Px += cn.mi[i].mx;
        Py += cn.mi[i].my;
        Pz += cn.mi[i].mz;
    }
    
    for (int i = 0; i < p->N_particles_total; i++)
    {
        //component in x
        cn.mi[i].mx = cn.mi[i].mx - Px;

        //component in y
        cn.mi[i].my = cn.mi[i].my - Py;

        //component in z
        cn.mi[i].mz = cn.mi[i].mz - Pz;
    }
    printf("Px = %lf, Py = %lf , Pz= %lf\n", Px, Py, Pz);
    return cn;
}

/*
*compute thermostat berendsen
*params: moments particles
*/
cinet_t compute_mc_berendsen(cinet_t cn, particles_t *p)
{
    for (int i = 0; i < p->N_particles_total; i++)
    {
        //component in x
        cn.mi[i].mx += gama*((T0/cn.Tc) - 1)*cn.mi[i].mx;

        //component in y
        cn.mi[i].my = gama*((T0/cn.Tc) - 1)*cn.mi[i].my;

        //component in z
        cn.mi[i].mz = gama*((T0/cn.Tc) - 1)*cn.mi[i].mz;
    }
    return cn;
}
//free memory after usage
void free_cinetique(cinet_t cn)
{
    free(cn.mi);
}