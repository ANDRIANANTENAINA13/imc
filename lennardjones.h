#ifndef _LENNARDJONES_H_
#define _LENNARDJONES_H_
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "tools.h"


//struct for the differents forces
typedef struct force_s
{
  double fx;
  double fy;
  double fz;
}force_t;
//struct for lennard jones
typedef struct lennard_s
{
  double Ulj;
  force_t **F;
  force_t *Fsum;
}lennard_t;


//function to compute the potential of Lennard Jones
lennard_t compute_Lennard_Jones(particles_t *p, double **r);

//print forces
void print_forces(lennard_t js, particles_t *p);
//print sum forces
void print_sum_forces(lennard_t js, particles_t *p);
//function periodic lennard jones
lennard_t compute_Lennard_Jones_periodic(particles_t *p, double **r, int N);
//function to verify force
void lennard_verify(lennard_t js, particles_t *p);

//function to free memory allocation
void free_lennard(lennard_t js, particles_t *p);

#endif
