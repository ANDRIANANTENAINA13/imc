#include <stdio.h>
#include <stdlib.h>
#include "lennardjones.h"
#include "velocity.h"
#include "tools.h"

int main(int argc, char **argv)
{

  //check argments
  if(argc < 5)
  {
    printf("Usage : [bin] [file] [STEP] [file output] [m_step]\n");
    exit(0);
  }

  char *fname = argv[1];
  int N_step = atoi(argv[2]);
  char *fname_out = argv[3];
  int m_step =atoi(argv[4]);
  //int test = atoi(argv[3]);
  
  particles_t *p = NULL;
  //lennard_t js;
  vec_t *tv;
  cinet_t Cn;
  double EnTo ;
  //double **r ;

  //read data in filename
  p = read_data(fname);

  //print data in teminal
  //print_data(p);
  tv = translator_vector_init(N_sym);
  //update particles
  //p = update_particle_data(p, tv, N_sym);
  //distance
  //r = compute_distance(p);
  //lennard Jones Non periodique Ulj = -2256.632207
  /*js =compute_Lennard_Jones(p , r);
  printf("Ulj = %lf\n", js.Ulj);

  lennard_verify(js, p);*/
  //Part for compute  Lennard Jones periodical*/
  //js = compute_Lennard_Jones_periodic(p, r, N_sym);

  //print forces
  //print_forces(js,p);
  /*printf("Periiodical Ulj = %lf\n", js.Ulj);
  lennard_verify(js, p);*/
  
  //print sum forces
  //print_sum_forces(js, p);
  
  printf("************* Dynamics molecular************\n");

   Cn = init_moment_cinetique(p);
   Cn = compute_cinetique_energie(Cn, p);
   Cn = compute_first_recalibrated(Cn, p);
   Cn = compute_second_recalibrated(Cn, p);
   Cn = compute_cinetique_energie(Cn, p);
   Cn = compute_first_recalibrated(Cn, p);

  for (int step = 0; step < N_step; step++)
  {
    Cn = compute_velocity_verlet(p, tv, N_sym, Cn, fname_out, N_step);
    Cn = compute_cinetique_energie(Cn, p);
  
    EnTo = Cn.Ec  + Cn.U;

    /*if (step%m_step != 0)
    {
      compute_mc_berendsen(Cn, p);
    }
    */
    printf("Step : %d, Etotal : %lf, Ec : %lf  ,  Ulj : %lf,  Tc : %lf \n",step,EnTo, Cn.Ec, Cn.U, Cn.Tc);
  }
  
  free_cinetique(Cn);
  free_vector_translate(tv);
  free_particle(p);
  return 0;
}
