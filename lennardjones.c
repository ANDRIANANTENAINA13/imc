#include "lennardjones.h"
#include <math.h>


/*compute Lennard Jones and forces
 *parms:particles p and distance r
 *return potential  Lennard Jones and force
*/
lennard_t compute_Lennard_Jones(particles_t *p, double **r)
{
  //check if pointer is NULL
  if(p == NULL)
  {
    printf("Error pointer is null\n" );
    exit(0);
  }
  double tmp = 0;

  lennard_t js;


  js.F=(force_t**)malloc(sizeof(force_t*)*p->N_particles_total);
  js.Fsum=(force_t*)malloc(sizeof(force_t)*p->N_particles_total);

  for (int i = 0; i < p->N_particles_total; i++)
  {
      js.F[i]=(force_t*)malloc(sizeof(force_t)*p->N_particles_total);
  }
    
//init force 
  for (int i = 0; i < p->N_particles_total; i++)
  {
    for (int j = 0; j < p->N_particles_total; j++)
    {
      //init Force each particles
      js.F[i][j].fx = 0.0;
      js.F[i][j].fy = 0.0;
      js.F[i][j].fz = 0.0;
    }

    //init sum forces
    js.Fsum[i].fx = 0.0;
    js.Fsum[i].fy = 0.0;
    js.Fsum[i].fz = 0.0;
  }

    for (int i = 0; i < p->N_particles_total; i++)
    {
      for (int j = i+1; j < p->N_particles_total; j++)
       {
        
           tmp +=((sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma)
           /(r[i][j] * r[i][j] * r[i][j] * r[i][j] * r[i][j] * r[i][j]))
           - 2*((sigma*sigma*sigma*sigma*sigma*sigma)/(r[i][j] * r[i][j] *r[i][j]));

           //compute of different forces
           js.F[i][j].fx = - 48*eps*(((sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma)
                      /(r[i][j]*r[i][j] * r[i][j] * r[i][j] * r[i][j] * r[i][j] * r[i][j]))
                      -((sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma)/
                      (r[i][j]*r[i][j]*r[i][j]*r[i][j])))*(p[i].coord.x - p[j].coord.x);
           js.F[i][j].fy = - 48*eps*(((sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma)
                      /(r[i][j]*r[i][j] * r[i][j] * r[i][j] * r[i][j] * r[i][j] * r[i][j]))
                      -((sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma)/
                      (r[i][j]*r[i][j]*r[i][j]*r[i][j])))*(p[i].coord.y - p[j].coord.y);
           js.F[i][j].fz = - 48*eps*(((sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma)
                      /(r[i][j]*r[i][j] * r[i][j] * r[i][j] * r[i][j] * r[i][j] * r[i][j]))
                      -((sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma)/
                      (r[i][j]*r[i][j]*r[i][j]*r[i][j])))*(p[i].coord.z - p[j].coord.z);

          //update force j with i
          js.F[j][i].fx =-js.F[i][j].fx;
          js.F[j][i].fy =-js.F[i][j].fy;
          js.F[j][i].fz =-js.F[i][j].fz;
         
       }
      }
    
    //compute lennard jones
    js.Ulj = 4*eps*tmp;

  return js;
}


//update distance particles
particles_t *update_particle_data(particles_t *p, vec_t *tv, int N)
{
  //check if pointer is null
  if (p == NULL && tv == NULL)
  {
    printf("Error pointer is null\n");
    exit(0);
  }
  
  particles_t *tmp = (particles_t*)malloc(sizeof(particles_t)*p->N_particles_total);
  tmp->N_particles_total = p->N_particles_total;

  for (int i = 0; i < N; i++)
  {
    for (int j = 0; j < tmp->N_particles_total; j++)
    {
      tmp[j].coord.x = p[j].coord.x + tv[i].x;
      tmp[j].coord.y = p[j].coord.y + tv[i].y;
      tmp[j].coord.z = p[j].coord.z + tv[i].z;
    }
  }
  
  return tmp;
}

/*
* compute lennard jones periodic
*
*/
lennard_t compute_Lennard_Jones_periodic(particles_t *p, double **r, int N)
{
  //check if pointer is NULL
  if(p == NULL && r == NULL)
  {
    printf("Error pointer is null\n" );
    exit(0);
  }
  double tmp = 0.0, sumfx = 0.0, sumfy = 0.0, sumfz = 0.0;

  lennard_t js;

  js.F=(force_t**)malloc(sizeof(force_t*)*p->N_particles_total);
  js.Fsum=(force_t*)malloc(sizeof(force_t)*p->N_particles_total);

  for (int i = 0; i < p->N_particles_total; i++)
  {
      js.F[i]=(force_t*)malloc(sizeof(force_t)*p->N_particles_total);
  }
  
  //init force 
  for (int i = 0; i < p->N_particles_total; i++)
  {
    for (int j = 0; j < p->N_particles_total; j++)
    {
      //init Force each particles
      js.F[i][j].fx = 0.0;
      js.F[i][j].fy = 0.0;
      js.F[i][j].fz = 0.0;
    }

    //init sum forces
    js.Fsum[i].fx = 0.0;
    js.Fsum[i].fy = 0.0;
    js.Fsum[i].fz = 0.0;
  }
  
  for (int i_sym = 0; i_sym < N; i_sym++)
  {
    for (int i = 0; i < p->N_particles_total; i++)
      {
        for (int j = i+1; j < p->N_particles_total; j++)
        {
          
          if (r[i][j] < R_cut*R_cut)
             { 
               tmp +=((sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma)
           /(r[i][j] * r[i][j] * r[i][j] * r[i][j] * r[i][j] * r[i][j]))
           - 2*((sigma*sigma*sigma*sigma*sigma*sigma)/(r[i][j] * r[i][j] *r[i][j]));

           //compute of different forces
           js.F[i][j].fx = - 48*eps*(((sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma)
                      /(r[i][j]*r[i][j] * r[i][j] * r[i][j] * r[i][j] * r[i][j] * r[i][j]))
                      -((sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma)/
                      (r[i][j]*r[i][j]*r[i][j]*r[i][j])))*(p[i].coord.x - p[j].coord.x);
           js.F[i][j].fy = - 48*eps*(((sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma)
                      /(r[i][j]*r[i][j] * r[i][j] * r[i][j] * r[i][j] * r[i][j] * r[i][j]))
                      -((sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma)/
                      (r[i][j]*r[i][j]*r[i][j]*r[i][j])))*(p[i].coord.y - p[j].coord.y);
           js.F[i][j].fz = - 48*eps*(((sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma)
                      /(r[i][j]*r[i][j] * r[i][j] * r[i][j] * r[i][j] * r[i][j] * r[i][j]))
                      -((sigma*sigma*sigma*sigma*sigma*sigma*sigma*sigma)/
                      (r[i][j]*r[i][j]*r[i][j]*r[i][j])))*(p[i].coord.z - p[j].coord.z);

          //update force j with i
          js.F[j][i].fx =-js.F[i][j].fx;
          js.F[j][i].fy =-js.F[i][j].fy;
          js.F[j][i].fz =-js.F[i][j].fz;    
             }
          
            
       }
    }
  }


  //update force 
  for (int i_sym = 0; i_sym < N; i_sym++)
  {
    for (int i = 0; i < p->N_particles_total; i++)
    {
      for (int j = 0; j < p->N_particles_total; j++)
      {
        //compute sum forces
            sumfx += js.F[i][j].fx;
            sumfy += js.F[i][j].fy;
            sumfz += js.F[i][j].fz;
      }

          //sum forces
          js.Fsum[i].fx += sumfx;
          js.Fsum[i].fy += sumfy;
          js.Fsum[i].fz += sumfz;
    }
  }
     double k = 0.5;

     if(N == 1)
        k = 1;

        //compute lennard jones
  js.Ulj = 4*k*eps*tmp;

  return js;
}

//print forces for each particles
void print_forces(lennard_t js, particles_t *p)
{
  for (int i = 0; i < p->N_particles_total; i++)
      {
        for (int j = 0; j < p->N_particles_total; j++)
        {
           printf("%lf  \t  %lf  \t  %lf", js.F[i][j].fx, js.F[i][j].fy, js.F[j][i].fz) ;
          
        }
        printf("\n");
       }
}

//print sum forces
void print_sum_forces(lennard_t js, particles_t *p)
{
  printf("**************Sum forces****************\n");
  for (int i = 0; i < p->N_particles_total; i++)
  {
    printf("Fx[%d] = %lf \t Fy[%d] = %lf  \t  Fz[%d] = %lf \n", i, js.Fsum[i].fx , i, js.Fsum[i].fy , i, js.Fsum[i].fz);
  }
  
}
//verification forces
void lennard_verify(lennard_t js, particles_t *p)
{
  double Fx = 0, Fy = 0, Fz = 0;

  for (int i = 0; i < p->N_particles_total; i++)
  {
    for (int j = 0; j < p->N_particles_total; j++)
    {
      Fx +=js.F[i][j].fx;
      Fy +=js.F[i][j].fy;
      Fz +=js.F[i][j].fz;
    }
  }

  printf("Fx = %lf  Fy = %lf  Fz = %lf\n ", Fx,Fy,Fz);
}
//free memory after usage
void free_lennard(lennard_t js, particles_t *p)
{
  for (int i = 0; i < p->N_particles_total; i++)
        free(js.F[i]);

      free(js.F);
      free(js.Fsum);
}
