#include "tools.h"

/*read data for coord particles
  param file: name of file
  return : table of coordinate particles
*/
particles_t *read_data(char *fname)
{

  //check if file is null
  if (fname == NULL)
  {
    printf("Error read data %s\n",__func__);
    exit(0);
  }
  FILE *file=fopen(fname, "r+");
  //check file
  if(!file)
    exit(1);
  int nb_elmt =0;
  int c;
  
  //check the number of element in file
  do
  {
    if(c=='\n')
      nb_elmt++;
  } while((c=fgetc(file))!=EOF);

  particles_t *p = (particles_t*)malloc(sizeof(particles_t)*nb_elmt);
 
  rewind(file);
  fscanf(file,"%d",&p->N_particles_total);

  //check if nb_particles_total different number of coordinate
  if (p->N_particles_total != nb_elmt-1)
  {
    printf("Number patricle is different of the number coordinates\n" );
    exit(0);
  }

  for (int i = 0; i < p->N_particles_total; i++)
  {
    fscanf(file,"  %d   %lf   %lf   %lf",&p[i].type,&(p[i].coord.x), &(p[i].coord.y),&(p[i].coord.z));
    
  }
  return p;
}

/*Function for print data
input param: particles
*/
void print_data(particles_t *p)
{
  //check if pointer is NULL
  if(p == NULL)
  {
    printf("Error pointer is null\n" );
    exit(0);
  }
  printf("P  \t| Type  \t |X \t\t |Y \t\t |Z\n");

  for (int i = 0; i < p->N_particles_total; i++)
  {
    printf("%d \t %d \t %lf \t %lf \t %lf\n",i,p[i].type,p[i].coord.x, p[i].coord.y,p[i].coord.z);
  }

  printf("=======================================================\n");
}

/*compute distance different particles
 *params: particles
 *return distance for the differents particles
*/
double **compute_distance(particles_t *p)
{
  //check if pointer is NULL
  if(p == NULL)
  {
    printf("Error pointer is null\n" );
    exit(0);
  }

   double **r = NULL;
   r=(double**)malloc(p->N_particles_total*sizeof(double*));
   for (int i = 0; i < p->N_particles_total; i++)
     r[i] =(double*)malloc(p->N_particles_total*sizeof(double));


  for (int i = 0; i < p->N_particles_total; i++)
  {
    for (int j = i+1; j < p->N_particles_total; j++)
    {
        //compute distance
        r[i][j] = ((p[i].coord.x - p[j].coord.x)*(p[i].coord.x - p[j].coord.x))+
                 ((p[i].coord.y - p[j].coord.y)*(p[i].coord.y - p[j].coord.y))+
                 ((p[i].coord.z - p[j].coord.z)*(p[i].coord.z - p[j].coord.z));
        //update  distance
        r[j][i] = r[i][j];
    }
  }

  return r;
}

/*traslator vector initialisation
*/
vec_t *translator_vector_init(int N)
{
  vec_t *tv = (vec_t*)malloc(sizeof(vec_t)*N);

  for (int i = 0; i < N; i++)
  {
    tv[i].x = ((double)((i / 9) - 1))*L;
    tv[i].y = ((double)((i / 3)% 3 -1))*L;
    tv[i].z = ((double)((i%3) - 1)) *L;
  }
  
  return tv;
}

//write the different output
void write_output_pdb(int xdym, int ydym, int zdym, char *fname_out, particles_t *p, int ITERATION)
{
  FILE *fname = fopen(fname_out,"a");

  fprintf(fname,"CRYST1  %d  %d  %d  90.00  90.00  90.00  P\n", xdym, ydym, zdym);
  fprintf(fname,"MODEL  %d\n", ITERATION);

  for (int i = 0; i < p->N_particles_total; i++)
  {
    fprintf(fname,"ATOM  %d  C  0  %lf  %lf  %lf      MRES\n",i, p[i].coord.x, p[i].coord.y, p[i].coord.z);
  }
  
  fprintf(fname,"TER \nENDMDL\n");
  fclose(fname);
}

//Detect number of neighbor

//free memory
void free_vector_translate( vec_t *tv)
{
  free(tv);
}

void free_distance(double **r, particles_t *p)
{
  for (int i = 0; i < p->N_particles_total; i++)
              free(r[i]);

  free(r);
}

void free_particle(particles_t *p)
{
  free(p);
}
