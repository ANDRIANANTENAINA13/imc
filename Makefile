CC=	gcc
LDFLAGS=	-lm
CFLAGS=	-o3	-g	-Wall
EXEC=	simu

all:	$(EXEC)

$(EXEC):	main.o lennardjones.o	velocity.o	tools.o
	$(CC)	$^	-o	$@	$(LDFLAGS)

%.o:%.c
	$(CC)	-c	$(CFLAGS)	$<	-o	$@

main.o:	main.c	lennardjones.h	velocity.h	tools.h
lennardjones.o:	lennardjones.c	lennardjones.h
velocity.o:	velocity.c	velocity.h
tools.o:	lennardjones.c	lennardjones.h
clean:
	rm	-rf *.o	$(EXEC)	*.pdb
